package com.gblfy.file.onlinepreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot启动器
 */
@SpringBootApplication
public class FileOnlinePreviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileOnlinePreviewApplication.class, args);
    }

}
